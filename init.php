<?php
    defined('DS') or define('DS', DIRECTORY_SEPARATOR);
    defined('PATH_CORE') or define('PATH_CORE', realpath(__DIR__.'..') . DS);

    include PATH_CORE.'classes'.DS.'AuthorisationHelper.php';
    include PATH_CORE.'classes'.DS.'Hook.php';
    include PATH_CORE.'classes'.DS.'Menu.php';
    include PATH_CORE.'classes'.DS.'PDOFactory.php';
    include PATH_CORE.'classes'.DS.'Router.php';
    include PATH_CORE.'classes'.DS.'TranslationHelper.php';

    include PATH_CORE.'classes'.DS.'abstract'.DS.'Page.php';
    include PATH_CORE.'classes'.DS.'abstract'.DS.'User.php';