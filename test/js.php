<html>
<head>
    <title>JS-Test</title>
</head>
<body id="mainBody">
    <h1>BASE</h1>

    <h2>stopEventHandling()</h2>
    <div data-click="alert('LAST');" style="display:flex;background-color:lightgrey;height:100px;width:300px;">
        <div data-stopEvent="" style="background-color:darkgrey;height:50px;width:300px;margin-top:10px;">
            <div data-click="alert('FIRST');" style="background-color:grey;height:50px;width:100px;margin-left:10px;">
                CLICK ME
            </div>
        </div>
    </div>

    <hr />
    <h1>DIALOG</h1>
    <button data-click="showDialog" data-id="dialog1" data-src="success.html">Open Dialog</button>

    <hr />
    <script src="//assets.privat/jquery.min.js"></script>
    <script src="/js/base.js?<?=filemtime('../js/base.js')?>"></script>
    <script src="/js/dialog.js?<?=filemtime('../js/dialog.js')?>"></script>
</body>
</html>