<?php
include '../init.php';
include PATH_CORE.'classes/Router.php';

echo 'Router: ';

Router::add('/user/', function() {
    echo 'Users';
});
Router::add('/user/(d+)/add/', function($arr) {
    echo 'User '.$arr[1].' added';
});
Router::add('/user/(d+)/delete/', function($arr) {
    echo 'User '.$arr[1].' deleted';
});
Router::add('/', function($arr) {
    echo 'HOME';
});

echo '<hr>';
echo Router::action('/user/31/delete/');
echo '<hr>';
echo Router::action('/user/31/add/');
echo '<hr>';
echo Router::action('/user/');
echo '<hr>';
echo Router::action('User/us');