<?php

    class Menu {
        private static $_aItems = array();
        
        public static function add($position, $name, $target = null, $icon = null, $parent = null) {
            $arr = array('position' => $position, 'name' => $name, 'target' => $target, 'icon' => $icon, 'parent' => $parent);
            if (!in_array($arr, self::$_aItems)) {
                self::$_aItems[] = $arr;
            }
        }

        public static function show() {
            usort(self::$_aItems, 'self::sortMenu');
            
            $result = '';
            foreach (self::$_aItems as $item) {
                if (is_null($item['parent'])) {
                    $sub    = '';
                    foreach (self::$_aItems as $item2) {
                        if ($item2['parent'] == $item['name']) {
                            if (strtolower($item2['name']) == 'separator') {
                                $sub .= '<div class="dropdown-divider"></div>';
                            } else {
                                $sub .= '<a class="dropdown-item" href="'.$item2['target'].'"><span>' . $item2['name'] . '</span></a>';
                            }
                        }
                    }
                    $result .= '<li class="nav-item'.((!empty($sub)) ? ' dropdown' : '').'">';
                    $result .= '<a class="nav-link'.((!empty($sub)) ? ' dropdown-toggle' : '').'" href="'.$item['target'].'" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
                    if (!is_null($item['icon'])) {
                        $result .= '<div class="' . $item['icon'] . '"></div>';
                    }
                    $result .= '<span>' . $item['name'] . '</span></a>';
                    if (!empty($sub)) {
                        $result .= '<div class="dropdown-menu" aria-labelledby="navbarDropdown">';
                        $result .= $sub;
                        $result .= '</div>';
                    }
                    $result .= '</li>';
                }
            }
            return $result;
        }

        public static function remove($name) {
            foreach (self::$_aItems as $item) {
                if ($item['name'] == $name) {
                    unset($item['name']);
                    break;
                }
            }
        }

        public static function list() {
            echo '<pre>'.print_r(self::$_aItems).'</pre>';
        }
        
        private static function sortMenu($a, $b) {
            return $a['position'] > $b['position'];
        }
    }