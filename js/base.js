$(function(){
    $('body').on('click', '[data-click]', clickAction);
    $('body').on('click', '[data-stopEvent]', stopAction);
    $('body').on('change', '[data-change]', changeAction);
});

if (self.$('#mainBody').length > 0){
    oMainPage = self;
} else if (parent.$('#mainBody').length > 0) {
    oMainPage = parent;
} else if (parent.parent.$('#mainBody').length > 0) {
    oMainPage = parent.parent;
} else {
    console.log('ERR0001');
}

function clickAction(event) {
    doAction(event, "click");
}

function changeAction(event) {
    doAction(event, "change");
}

function doAction(event, type) {
    if (typeof type == "undefined") {
        type = "action"
    }
    var oData	= $(event.currentTarget);
    var sAction	= $(oData).data(type);

    if (!$(oData).hasClass('noPreventDefault')) {
        event.preventDefault();
    }

    if (typeof sAction === 'object') {
        // TRIGGER multiple actions by object
        $.each(sAction, function(i, subAction) {
            if (typeof(window[subAction]) === "function") {
                setTimeout( function(){window[subAction](oData);}, 500 * i);
            }
        });
    } else {
        // TRIGGER action by param
        if (sAction !== undefined && sAction) {
            if (typeof(window[sAction]) === "function") {
                window[sAction](oData);
            } else if (eval("typeof "+sAction) === "function") {
                eval(sAction+"(oSrc)");
            }
        }
    }

    return false;
}

function stopAction(event) {
    if (event && event.stopPropagation) {
        event.stopPropagation();
    } else if (window.event) {
        window.event.cancelBubble=true;
    }
}

function loading(visible) {
    if (typeof visible !== "bool") {
        visible = false;
    }
    if ($('#loader').length < 0) {
        $('body').prepend('<div id="loader"></div>');
    }

    if (visible) {
        $('#loader').fadeIn();
    } else {
        $('#loader').fadeOut();
    }
}