<?php
namespace Core;

abstract class Page {
    protected $title = '';
    protected $meta = array();
    protected $scripts = array();
    protected $styles = array();
    protected $body = '';

    public function __construct() {
        $this->addMeta('viewport', 'width=device-width, initial-scale=1.0');
    }

    public function setTitle($title) {
        $this->title = $title;
    }

    public function addMeta($name, $content) {
        if (!empty($name) and !empty($content)) {
            $this->meta[] = array('name' => $name, 'content' => $content);
        }
    }

    public function getMeta() {
        $result = '';
        foreach ($this->meta as $meta) {
            $result .= '<meta name="'.$meta['name'].'" content="'.$meta['content'].'">';
        }
        return $result;
    }

    public function addScripts($src) {
        if (!empty($src)) {
            $this->scripts[] = $src;
        }
    }

    public function getScripts() {
        $result = '';
        foreach ($this->scripts as $script) {
            $result .= '<script src="'.$script.'"></script>';
        }
        return $result;
    }

    public function addStyles($src) {
        if (!empty($src)) {
            $this->styles[] = $src;
        }
    }

    public function getStyles() {
        $result = '';
        foreach ($this->styles as $style) {
            $result .= '<link href="'.$style.'" rel="stylesheet">';
        }
        return $result;
    }

    public function setBody($html) {
        $this->body = $html;
    }

    public function addBody($html) {
        $this->body .= $html;
    }

    public function displayPage() {
        $page = '<!DOCTYPE html>
<html>
    <head>
        <title>'.$this->title.'</title>
        <meta charset="UTF-8">
        '.$this->getStyles().'
    </head>
    <body>
        '.$this->body.'
        '.$this->getScripts().'
    </body>
</html>
';
        echo $page;
    }
}