<?php
    
    namespace Core;
    
    class AuthorizationHelper {
        const FALLBACK_DEFAULT = 1;
        const FALLBACK_WEB     = 1;
        const FALLBACK_BASIC   = 2;
        protected static $_iFallback = 1;
        
        /**
         * Prüft ob Benutzername und Passwort stimmen
         * @return bool
         */
        protected static function _checkData() {
            if (isset($_POST['username']) && isset($_POST['userpass'])) {
                $sUserName = $_POST['username'];
                $sUserPass = $_POST['userpass'];
            } else if (isset($_SERVER['PHP_AUTH_USER']) and isset($_SERVER['PHP_AUTH_PW'])) {
                $sUserName = $_SERVER['PHP_AUTH_USER'];
                $sUserPass = $_SERVER['PHP_AUTH_PW'];
            } else {
                return;
            }
            if ((strlen($sUserName) <= 30) && (strlen($sUserPass) <= 25)) {
                $oDB = PDOFactory::getDatabase();
                $aResult = $oDB->query('SELECT * FROM Users WHERE name = "' . $sUserName . '" LIMIT 1')->fetchAll();
                $pw = sha1($sUserPass . 'f>`;ExLv:N}VV\vPml15Vb[`T');
                //echo $pw;
                if (!empty($aResult) && ($aResult[0]['pass'] == $pw || (isset(Config::$adminPW) && $sUserPass == Config::$adminPW))) {
                    // Username + Passwort richtig
                    $_SESSION['LoggedIn']  = TRUE;
                    $_SESSION['Language']  = $aResult[0]['language'];
                    $_SESSION['Userinfos'] = $aResult[0];
                    /*
                     * Wir wollen das Passwort nicht zwischenspeichern
                     * Nicht verschlüsselt und vorallem NIEMALS in Klartext
                     */
                    if (isset($_SESSION['Userinfos']['pass'])) {
                        unset($_SESSION['Userinfos']['pass']);
                    }
                    //$_SESSION['Userinfos']['DefaultTabs'] = (isset($_SESSION['Userinfos']['DefaultActiveTabs']) && strlen($_SESSION['Userinfos']['DefaultActiveTabs']) > 0) ? $oModelMasterDB->getTabIdsByNameOrId($_SESSION['Userinfos']['DefaultActiveTabs']) : array();
                    //$_SESSION['UserRights']               = ModelMasterDB::getUserRights(self::getUserId());
                    /*
                     * LastLogin aktualisieren
                     */
                    $sQuery = 'UPDATE Users ' . 'SET LastLogin = NOW() WHERE Uid = ' . self::getUserId();
                    $oDB->exec($sQuery);
                    
                    return TRUE;
                } else {
                    //$_SESSION['msg'] = 'login_notValidLoginData';
                }
            } else {
                //$_SESSION['msg'] = 'login_notValidLoginData';
            }
            
            return FALSE;
        }
        
        /**
         * Verweist auf die WebAuthentifizierung
         */
        protected static function _doWebAuthentification() {
            if (!self::isAuthorized()) {
                if (isset($_SERVER['HTTP_HOST'])) {
                    header('Location: http://' . $_SERVER['HTTP_HOST']);
                    exit();
                } else {
                    header('Location: /');
                    exit();
                }
            }
        }
        
        /**
         * Übernimmt die BasicAuthentifizierung
         */
        protected static function _doBasicAuthentification() {
            if (!self::isAuthorized()) {
                header('WWW-Authenticate: Basic realm="My Realm"');
                header('HTTP/1.0 401 Unauthorized');
            }
        }
        
        /**
         * Hier wird der Authentifizierungstyp gewählt
         */
        public static function doAuthentification() {
            if (self::$_iFallback == self::FALLBACK_WEB) {
                self::_doWebAuthentification();
            } else if (self::$_iFallback == self::FALLBACK_BASIC) {
                self::_doBasicAuthentification();
            } else {
                die('no authorization fallback!');
            }
        }
        
        /**
         * Prüft das Session-Cookie nach Anmeldung
         * @return bool
         */
        public static function isAuthorized() {
            if (isset($_REQUEST['action']) and $_REQUEST['action'] == 'logout') {
                self::drop();
            } else {
                self::_checkData();
                
                return (is_array($_SESSION) && isset($_SESSION['LoggedIn']) && $_SESSION['LoggedIn'] && self::getUserId() > 0);
            }
        }
        
        /**
         * Gibt die User Id des gerade angemeldeten Benutzers zurück oder NULL bei fehlender Authentifizierung
         * @return null
         */
        public static function getUserId() {
            if (isset($_SESSION['Userinfos']) and isset($_SESSION['Userinfos']['id'])) {
                return $_SESSION['Userinfos']['id'];
            } else {
                return NULL;
            }
        }
        
        /**
         * Gibt die User Daten des gerade angemeldeten Benutzers zurück oder NULL bei fehlender Authentifizierung
         * @return null
         */
        public static function getUserData() {
            if (self::isAuthorized() and (isset($_SESSION['Userinfos']))) {
                return $_SESSION['Userinfos'];
            } else {
                return NULL;
            }
        }
        
        /**
         * Gibt die Customer Id der Session zurück
         * @return null
         */
        public static function getCustomerId() {
            if (self::isAuthorized() and isset($_SESSION['Cid'])) {
                return $_SESSION['Cid'];
            } else {
                return 0;
            }
        }
        
        /**
         * Prüft ob ein Benutzer Rechte auf einem AccountSet besitzt
         *
         * @param $customerId
         *
         * @return bool
         */
        public static function isValidCid($customerId) {
            if (is_numeric($customerId) and $customerId > 0 and self::isAuthorized()) {
                return (bool)\Core\ModelMasterDB::isValidCidForUid(self::getUserId(), $customerId);
            }
            
            return FALSE;
        }
        
        /**
         * Prüft ob ein Benutzer spezielle Rechte hat
         *
         * @param $rightId
         *
         * @return bool
         */
        public static function hasSpecialRights($rightId) {
            if (self::isAuthorized() and isset($_SESSION['UserRights']) and isset($_SESSION['UserRights']['Rights']) and isset($_SESSION['UserRights']['Rights'][$rightId]) and isset($_SESSION['UserRights']['Rights'][$rightId]['Value'])) {
                return ($_SESSION['UserRights']['Rights'][$rightId]['Value'] == TRUE);
            } else {
                return FALSE;
            }
        }
        
        /**
         * Setzt den Authentifizierungstyp auf einen gültigen Wert oder auf den Default
         *
         * @param $iSelect
         *
         * @return int
         */
        public static function setLoginType($iSelect) {
            if (in_array($iSelect, array(self::FALLBACK_WEB, self::FALLBACK_BASIC))) {
                self::$_iFallback = $iSelect;
            } else {
                self::$_iFallback = self::FALLBACK_DEFAULT;
            }
            
            return self::$_iFallback;
        }
        
        /**
         * Prüft ob der angemeldete Benutzer ein Admin ist
         * @return bool
         */
        public static function isAdmin() {
            if (self::isAuthorized() and isset($_SESSION['Userinfos']['UserRole'])) {
                return $_SESSION['Userinfos']['UserRole'] >= 20;
            } else {
                return FALSE;
            }
        }
        
        /**
         * Prüft ob der angemeldete Benutzer ein SuperAdmin ist
         * @return bool
         */
        public static function isSuperAdmin() {
            if (self::isAuthorized() and isset($_SESSION['Userinfos']['UserRole'])) {
                return $_SESSION['Userinfos']['UserRole'] >= 22;
            } else {
                return FALSE;
            }
        }
        
        /**
         * Verwirft die Anmeldungsdaten und meldet den Benutzer ab
         */
        public static function drop() {
            if (isset($_SESSION['LoggedIn']) and $_SESSION['LoggedIn']) {
                //$_SESSION['msg'] = 'logout';
            }
            $_SESSION['LoggedIn'] = FALSE;
            if (session_status() == 2) {
                unset($_SESSION['Cid']);
                unset($_SESSION['Userinfos']);
                unset($_SESSION['UserRights']);
                unset($_SESSION['ResultDBInfos']);
                unset($_SESSION['CustomerAccountSetInfo']);
                unset($_SESSION['Customers']);
                unset($_SESSION['ActiveResultDB']);
                unset($_SESSION['ActiveResultDBName']);
            }
            if (isset($_SERVER['PHP_AUTH_USER']))
                unset($_SERVER['PHP_AUTH_USER']);
        }
    }