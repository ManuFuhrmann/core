<?php
namespace Core;

class Router {
    private static $routes;
    private static $side404 = '404';

    public static function add($name, $func, $bool = TRUE) {
        $raw = $name;
        if ($bool) {
            $name = str_replace('/', '\\/', $name);
        }
        $name = str_replace('d+', '[0-9]{1,}', $name);
        self::$routes[] = array('raw' => $raw, 'name' => $name, 'func' => $func);
    }

    public static function remove($name) {
        foreach (self::$routes as &$route) {
            if ($route['raw'] == $name) {
                unset($route);
                return TRUE;
            }
        }
        return FALSE;
    }

    public static function action($path) {
        if (is_array(self::$routes)) {
            foreach (self::$routes as $route) {
                preg_match('&^' . $route['name'] . '$&', $path, $matches);
                if (!empty($matches)) {
                    $arr = explode(':', $route['func']);
                    
                    if (is_callable($route['func'])) {
                        return $route['func']($matches);
                    } else if (method_exists($arr[0], $arr[1])) {
                        return $route['func']($matches);
                    } else {
                        return '501';
                    }
                }
            }
        }
        return self::$side404;
    }

    public static function show() {
        echo '<pre>'.print_r(self::$routes).'</pre>';
    }
    
    public static function set404($html) {
        self::$side404 = $html;
    }
}