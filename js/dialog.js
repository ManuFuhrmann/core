function showDialog(oConfigIn) {
    /*
    # require
        data-id*:           set id from data attribute
        data-src*:          set HTML source (iFrame URL or templateID)

    # optional
        data-config         combines all needed params in single object
        data-version        if anytime the function must be updated
        data-ownheader:     dialog without default header
        data-title:         set title from data attribute
        data-class:         set class from data attribute
        data-width:         set layer width from data attribute
        data-maxheight:     set layer max height
        data-type:          set layer type ["iframe",""] (HTML-Template oder iFrame-Url als Quelle für Dialog)
        data-object:        object for i do not know right now
    */

    var oConfig = {};
    oConfig.id = $(oConfigIn).data('id');
    oConfig.src = $(oConfigIn).data('src');

    oConfig.version = ($(oConfigIn).data('version') !== undefined) ? Number(oConfigIn.data('version')) : 0;
    oConfig.ownHeader = $(oConfigIn).data('ownheader');
    oConfig.title = ($(oConfigIn).data('title') !== undefined) ? oConfigIn.data('title') : '';
    oConfig.class = ($(oConfigIn).data('class') !== undefined) ? ' '+oConfigIn.data('class') : '';
    oConfig.width = ($(oConfigIn).data('width') !== undefined) ? Number(oConfigIn.data('width')) : 0;
    oConfig.maxHeight = ($(oConfigIn).data('maxheight') !== undefined) ? Number(oConfigIn.data('maxheight')) : 0;
    oConfig.type = $(oConfigIn).data('type');
    oConfig.object = $(oConfigIn).data('object');

    // EXTEND oConfig by data-config object
    if ($(oConfigIn).data('config')){
        $.extend( true, oConfig, $(oConfigIn).data('config') );
    }

    // CHECK required params
    if ($.isEmptyObject(oConfig.id) === true) {
        console.log('no dialog id');
        return 'no dialog id';
    }
    if ($.isEmptyObject(oConfig.src) === true) {
        console.log('no dialog content source');
        return 'no dialog content source';
    }

    // SET title link
    if ($.isEmptyObject(oConfig.title) === false) {
        oConfig.title = '<span class="whiteFont"> '+oConfig.title+'</span>';
    }

    loading();

    // SET z-index
    var iDialogNum = $('body').find('dialog').length;
    if ($.isEmptyObject(oConfig.ownHeader) === false) {
        var sDefaultHeader = '';
    }
    /* OwnHeader

    <div id="titleBar" class="dialogTitle title flexBox">
        <span class="data-title">Weiterleitungskette für AAid 584187 - Aff-Tag bonprix. in DisplayUrl. brandviolation detected.</span>
        <div class="title-buttons flexBox">
            <button type="button" data-action="followAd" data-source="584187" class="layerButton redBG fn_click">Beobachten</button><br class="clear">
            <button type="button" name="saveAdLayer" data-action="saveAdLayer" class="layerButton fn_click">Speichern</button>
            <button type="button" name="saveDelete" data-action="saveDeleteAd" data-layercid="3093" data-layeraaid="584187" class="layerButton greenBG fn_click">Speichern&amp;Entfernen</button>
            <a class="fn_click icon icon-close icon-white hover-red" data-action="closeDialog"></a>
        </div>
    </div>
    */
    var oDialog = $('<dialog></dialog>', {'id': oConfig.id, 'data-click': 'closeDialog', 'data-state': 'loading', 'style': 'position:fixed;height:100%;width:100%;top:0;left:0;background:rgba(0,0,0,.8);border:0;padding:0;align-items:center;justify-content:center;display:flex;'});
    var oSection = $('<section></section>', {'data-stopAction': '1', 'style': 'background:white;width:500px;margin:auto;'});
    oDialog.append(oSection[0].outerHTML);
    $('body').prepend(oDialog[0].outerHTML);
    // SET dialog type
    if (oConfig.type === 'iframe') {
        $('dialog section').append('<iframe id="mod_iframe" name="mod_iframe" class="layer" src="'+oConfig.src+'" width="100%" frameborder="0" scrolling="no" onload="resizeIframe(this); loading(false); $(this).closest("dialog").attr("data-state","ready"); $(this).closest("dialog").attr("open","open");"></iframe>');
    } else {// if (oConfig.type === "embedded")
        // GET content source (depending on tagname)
        $.ajax({method: 'POST',
            url: oConfig.src,
            success: function (data) {
                $('dialog#' + oConfig.id + ' section').append(data);
                $('dialog#' + oConfig.id).attr("data-state","ready");
                $('dialog#' + oConfig.id).attr("open","open");
                loading(false);
            }
        });
        return false;
    }

    // REPLACE dynamic placeholders
    if ($.isEmptyObject(oConfig.dataObject) === false) {
        $.each(oConfig.dataObject, function(key, value){
            sContent = sContent.replace(new RegExp('%%' + key + '%%', 'g'), value);
        });
    }

    return false;
}

function closeDialog(oData) {
    var sDialog = 'dialog';
    if (typeof oData.id == 'string') {
        sDialog += '#' + oData.id;
    }
    $(sDialog).fadeOut();
    $(sDialog).remove();
}