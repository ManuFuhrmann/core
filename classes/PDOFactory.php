<?php
    namespace Core;
    
    use \PDO;
    
    class PDOFactory extends PDO {
        protected static $instances = array();
        
        public static function getDatabase() {
            if (defined('PATH_MAIN') and file_exists(PATH_MAIN . 'config/db.ini')) {
                $parsed = parse_ini_file(PATH_MAIN . 'config/db.ini');
            } else if (defined('PATH_MAIN') and file_exists(PATH_MAIN . 'config/db.ini')) {
                $parsed = parse_ini_file(PATH_MAIN . 'config/db.ini');
            } else {
                return FALSE;
            }
            
            $host     = $parsed['host'];
            $username = $parsed['user'];
            $password = $parsed['pass'];
            $database = $parsed['database'];
            $tag      = $username . '@' . $host;
            if (!isset(self::$instances[$tag])) {
                $pdo = new self('mysql:dbname=' . $database . ';host=' . $host . ';charset=UTF8', $username, $password);
                self::$instances[$tag] = $pdo;
            }
            
            return self::$instances[$tag];
        }
        
        public function query($query, $type = PDO::FETCH_ASSOC) {
            return parent::query($query, $type);
        }
        
        public function exec($query) {
            return parent::exec($query);
        }
        
        public function fetch($type = \PDO::FETCH_ASSOC) {
            return parent::fetch($type);
        }
        
        public function fetchAll($type = \PDO::FETCH_ASSOC) {
            return parent::fetchAll($type);
        }
    }