<?php

    class Hook {
        private static $_aHooks = array();

        public static function add($channel, $func){
            if (!isset(self::$_aHooks[$channel])) {
                self::$_aHooks[$channel] = array();
            }

            self::$_aHooks[$channel][] = $func;
        }

        public static function run($channel) {
            if (isset(self::$_aHooks[$channel])) {
                foreach (self::$_aHooks[$channel] as $func) {
                    $funcResult = $func();
                    if (is_array($funcResult)) {
                        if (!isset($result) or count($result) == 0) {
                            $result = array();
                        }
                        $result[] = $funcResult;
                    } else if (is_string($funcResult)) {
                        if (empty($result) or !is_array($result)) {
                            $result = '';
                        }
                        $result .= $funcResult;
                    }
                }
            }
            return $result;
        }

        public static function remove($channel) {
            if (isset(self::$_aHooks[$channel])) {
                unset(self::$_aHooks[$channel]);
            }
        }

        public static function list() {
            echo '<pre>'.print_r(self::$_aHooks).'</pre>';
        }
    }