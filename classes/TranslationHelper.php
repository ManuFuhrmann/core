<?php
    namespace Core;
    
    use \Core\PDOFactory;
    use \PDO;
    
    class TranslationHelper {
        /* Singleton START */
        protected static $_instance = NULL;
        
        protected function __clone() {
        }
        
        protected function __construct() {
        }
        
        protected static function getInstance() {
            if (!isset(self::$instance)) {
                self::$_instance = new self;
            }
            
            return self::$_instance;
        }
        
        /* Singletonpart END */
        protected $_bDebugMode = FALSE;
        const DEFAULT_LANGUAGE = 'de';
        
        protected function _getTranslationData() {
            if (($oDB = PDOFactory::getDatabase()) !== FALSE) {
                $oQuery = $oDB->query('SELECT * FROM `translation`');

                $aResult = $oQuery->fetchAll();

                return $aResult;
            }
            return array();
        }
        
        protected function getTranslationTable() {
            if (isset($_SESSION['translations']) and is_array($_SESSION['translations']) and 0 < count($_SESSION['translations'])) {
                return $_SESSION['translations'];
            } else {
                $aLanguageData = $this->_getTranslationData();
                $aTranslations = array();
                foreach ($aLanguageData as $aTextPart) {
                    if ($aTextPart['name'] == '') {
                        continue;
                    }
                    $translationName = $aTextPart['name'];
                    $translation     = array();
                    unset($aTextPart['name']);
                    foreach ($aTextPart as $key => $value) {
                        $translation[$key] = $value;
                    }
                    $aTranslations[$translationName] = $translation;
                    unset($translation);
                }
                if (!$this->_bDebugMode) {
                    $_SESSION['translations'] = $aTranslations;
                }
                
                return $aTranslations;
            }
        }
        
        public static function setDebugMode($bool = FALSE) {
            $oTranslationHelper              = self::getInstance();
            $oTranslationHelper->_bDebugMode = $bool;
        }
        
        public static function setTranslationCode($sLanguageCode = NULL) {
            $sLanguageCode = strtolower($sLanguageCode);
            //clean up 'de-DE' to de
            if (stripos($sLanguageCode, '-') !== FALSE) {
                $sLanguageCode = substr($sLanguageCode, 0, stripos($sLanguageCode, '-'));
            }
            $aResult = PDOFactory::getDatabase()->query('SELECT * FROM `translation` LIMIT 1')->fetchAll();
            if (array_key_exists($sLanguageCode, $aResult[0])) {
                $_SESSION['Language'] = $sLanguageCode;
                
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        public static function getTranslationCode() {
            if (!isset($_SESSION['Language']) and !self::_getBrowserLanguage()) {
                $_SESSION['Language'] = self::DEFAULT_LANGUAGE;
            }
            
            return $_SESSION['Language'];
        }
        
        public static function getTranslation($sKey, $aReplacments = array(), $sLanguageCode = '') {
            $oTranslationHelper = self::getInstance();
            $aTranslations      = $oTranslationHelper->getTranslationTable();
            if (isset($aTranslations[$sKey])) {
                if (!empty($sLanguageCode)) {
                    $aResult = PDOFactory::getDatabase()->query('SELECT * FROM `translation` LIMIT 1')->fetchAll();
                    if (array_key_exists($sLanguageCode, $aResult[0])) {
                        $sTranslation = $aTranslations[$sKey][$sLanguageCode];
                    }
                }
                if (!isset($sTranslation)) {
                    if (!is_null($aTranslations[$sKey][self::getTranslationCode()])) {
                        $sTranslation = $aTranslations[$sKey][self::getTranslationCode()];
                    } else if (!is_null($aTranslations[$sKey][$oTranslationHelper::DEFAULT_LANGUAGE])) {
                        $sTranslation = $aTranslations[$sKey][$oTranslationHelper::DEFAULT_LANGUAGE];
                    } else {
                        $sTranslation = $sKey;
                    }
                }
            } else {
                $sTranslation = $sKey;
            }
            if (is_array($aReplacments)) {
                foreach ($aReplacments as $key => $value) {
                    $sTranslation = str_replace('%%' . $key . '%%', $value, $sTranslation);
                }
            }
            
            return $sTranslation;
        }
        
        public static function _getBrowserLanguage() {
            if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
                $sLanguages = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
                $aLanguages = explode(',', $sLanguages);
                $result     = array();
                foreach ($aLanguages as $value) {
                    $arr = explode(';', $value);
                    if (isset($arr[1])) {
                        $result[str_replace('q=', '', $arr[1]) * 100] = $arr[0];
                    } else {
                        $result[100] = $arr[0];
                    }
                }
                krsort($result);
                foreach ($result as $languageCode) {
                    if (self::setTranslationCode($languageCode)) {
                        return TRUE;
                    }
                }
            }
            
            return FALSE;
        }
    }